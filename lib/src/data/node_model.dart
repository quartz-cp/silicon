/// A [Node] is a physical server which can in itself host multiple [Server]s.
class Node {
  final int id;
  final String name;
  final String host;

  Node(this.id, this.name, this.host);
}
