class ServerStatus {
  final int ping;
  final String description;
  final _ServerStatusPlayers players;
  final String favicon;
  final _ServerStatusVersion version;
  final int lastUpdated;

  ServerStatus(this.ping, this.description, this.players, this.favicon,
      this.version, this.lastUpdated);
}

class _ServerStatusPlayers {
  final int max;
  final int online;
  final List<String> sample;

  _ServerStatusPlayers(this.max, this.online, this.sample);
}

class _ServerStatusVersion {
  final String name;
  final int protocol;

  _ServerStatusVersion(this.name, this.protocol);
}
