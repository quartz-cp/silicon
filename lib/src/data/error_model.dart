/// An object describing an error which has occurred in the Quartz namespace.
class Error {
  /// A message detailing the error.
  final String error;

  Error(this.error);
}
