class Player {
  /// The unique identifier for this player.
  ///
  /// The [id] should be persistent across changes to other properties related
  /// to this player, such as their [name].
  ///
  /// In Minecraft this property is equivalent to their UUID which can be found
  /// using online tools such as [Minecraft UUID](https://mcuuid.net).
  final String id;

  /// The name of this player in-game.
  ///
  /// In Minecraft this property is equivalent to their username.
  final String name;

  Player(this.name, this.id);
}
