/// A [Server] object represents a game server, with the [name], [host] and
/// [port] provided by the server administrator when created or registered with
/// Houston through the control panel.
///
/// The [port] property defaults to 25565, which is the standard port for
/// Minecraft servers. The vanilla client makes connections on this port if no
/// other is provided.
///
/// The [id] property is unique for each server known to Houston.
class Server {
  /// The ID of the server in Houston.
  final int id;

  /// The name of this server, set by the server owner.
  final String name;

  /// The domain name or IP address of this server.
  final String hostname;

  /// The port on which the server is serving connections. A number between 0
  /// and 65535.
  final int port;

  Server(this.id, this.name, this.hostname, {this.port = 25565});
}
